<?php
require_once 'connection.php'; // подключаем скрипт

$link = mysqli_connect($host, $user, $password, $database) 
	or die("Ошибка " . mysqli_error($link));

//CREATE TABLES

$sql = "CREATE TABLE client (
	id_client int(10) auto_increment,	
	name_client varchar(255) not null,
	adress_client varchar(255) not null,
	phone_client int(11) not null,
	email_client varchar(255) not null,
	personal_discount int(3) not null,
	primary key (id_client)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE flowers (
	id_flower int(10) auto_increment,
	name_flower varchar(255) not null,
	price_flower int(10) not null,
	accounting int(10) not null,
	factual int(10) not null,
	conditionals varchar(300) not null,
	primary key (id_flower)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE provider (
	id_provider int(10) auto_increment,
	name_provider varchar(255) not null,
	adress_provider varchar(255) not null,
	phone_provider int(11) not null,
	email_provider varchar(255) not null,
	primary key (id_provider)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE services (
	id_service int(10) auto_increment,
	description varchar(255) not null,
	price_service int(10) not null,
	primary key (id_service)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE worker (
	id_worker int(10) auto_increment,
	position varchar(255) not null,
	name_worker varchar(255) not null,
	experience varchar(255) not null,
	phone_worker int(11) not null,
	salary int(10) not null,
	schedule varchar(255) not null,
	primary key (id_worker)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE ord (
	id_order int(10) auto_increment,
	id_client int(10) not null,
	date_order date not null,
	cost_order int(10) not null,
	quantity_flower int(10) not null,
	id_flower int(10) not null,
	id_service int(10) not null,
	primary key (id_order),
	foreign key (id_client) references client (id_client),
	foreign key (id_flower) references flowers (id_flower),
	foreign key (id_service) references services (id_service)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

$sql = "CREATE TABLE providing (
	id_provider int(10) not null,
	id_flower int(10) not null,
	number_flowers int(10) not null,
	piece_price int(10) not null,
	full_price int(10) not null,
	date_providing date not null,
	foreign key (id_provider) references provider (id_provider),
	foreign key (id_flower) references flowers (id_flower)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully<br>";
} else {
	echo "Error creating table: " . mysqli_error($link) . "<br>";
}

//INSERT PROVIDER


$query = "INSERT INTO provider VALUES(1, 'ЦветОптТорг','ул. Выборгская, д. 5', 123, 'copt@mail.ru')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO provider VALUES(2, 'Цветочкин','ул. Ленина, д. 23', 139, 'cvetochki@mail.ru')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO provider VALUES(3, 'Flowers','ул. Рубинштейна, д. 56', 389, 'rubiflowers@mail.ru')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO provider VALUES(4, 'Semicvetic','ул. Стремянная, д. 193', 704, 'Semicvetic@mail.ru')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO provider VALUES(5, 'Ботаника','ул. Оптовиков, д. 29', 717, 'botanika@mail.ru')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT WORKERS

$query = "INSERT INTO worker VALUES(1, 'директор','Прокашева Марина Владимировна', '10 лет', 317, 70000, 'свободный')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO worker VALUES(2, 'администратор','Павлюченко Ксения Вячеславовна', '14 месяцев', 298, 31000, '5/2')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO worker VALUES(3, 'флорист','Коучев Денис Кириллович', '8 месяцев', 832, 28000, '2/2')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO worker VALUES(4, 'доставщик','Козлов Роман Васильевич', 'без стажа', 933, 19000, '5/2')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT CLIENT

$query = "INSERT INTO client VALUES(1, 'Пупыркина Мария Владимировна','ул. Пионерская, д. 67', 291, '@mail', 0)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO client VALUES(2, 'Камышева Ольга Дмитриевна','ул. Водокональная д. 45', 738, '@mail', 5)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO client VALUES(3, 'Лиляев Антон Геннадьевич','ул. Пушкина, д. 1', 109, '@mail', 3)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO client VALUES(4, 'Самураев Николай Алексеевич','ул. Суворова, д. 303', 032, '@mail', 20)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO client VALUES(5, 'Глинка Светлана Петровна','ул. Достоевского, д. 78', 382, '@mail', 0)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT FLOWERS

$query = "INSERT INTO flowers VALUES(1, 'роза красная 40 см', 80, 34, 34, 'холодильник')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO flowers VALUES(2, 'роза желтая 40 см', 80, 18, 18, 'холодильник')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO flowers VALUES(3, 'роза синяя 40 см', 100, 7, 5, 'холодильник')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO flowers VALUES(4, 'роза розовая 40 см', 80, 27, 27, 'холодильник')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO flowers VALUES(5, 'роза кустовая', 120, 11, 14, 'холодильник')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT SERVICES

$query = "INSERT INTO services VALUES(1, 'без услуг', 0)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO services VALUES(2, 'оформление букета', 200)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO services VALUES(3, 'оформление помещения', 7000)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO services VALUES(4, 'доставка', 300)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT ORD

$query = "INSERT INTO ord VALUES(1, 1, '2018-02-18', 920, 9, 1, 2)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO ord VALUES(2, 3, '2018-02-18', 1900, 17, 2, 2)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO ord VALUES(3, 3, '2018-02-18', 300, 1, 1, 3)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO ord VALUES(4, 2, '2018-02-18', 1350, 5, 4, 2)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO ord VALUES(5, 4, '2018-02-18', 918, 51, 3, 2)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT PROVIDING

$query = "INSERT INTO providing VALUES(1, 1, 50, 50, 2500, '2018-02-17')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO providing VALUES(1, 2, 50, 50, 2500, '2018-02-17')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO providing VALUES(2, 3, 40, 30, 1200, '2018-02-17')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO providing VALUES(3, 4, 70, 50, 3500, '2018-02-17')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO providing VALUES(5, 5, 90, 100, 9000, '2018-02-17')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

mysqli_close($link);
?>
